/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://starter/./src/scss/style.scss?");

/***/ }),

/***/ "./src/js/accordion.js":
/*!*****************************!*\
  !*** ./src/js/accordion.js ***!
  \*****************************/
/***/ (() => {

eval("const accordionItems = document.querySelectorAll(\".js-accordion-button\");\r\n\r\nfunction accordionDropdown() {\r\n  const accordionItem = this;\r\n  const accordionBody = accordionItem.nextElementSibling;\r\n  const plusIcon = accordionItem.querySelector(\".plus\");\r\n  const minusIcon = accordionItem.querySelector(\".minus\");\r\n\r\n  // Close all other accordion items\r\n  accordionItems.forEach((item) => {\r\n    if (item !== accordionItem && item.classList.contains(\"active\")) {\r\n      item.classList.remove(\"active\");\r\n      const body = item.nextElementSibling;\r\n      const otherPlusIcon = item.querySelector(\".plus\");\r\n      const otherMinusIcon = item.querySelector(\".minus\");\r\n\r\n      body.style.maxHeight = null;\r\n      otherPlusIcon.classList.remove(\"hidden\");\r\n      otherMinusIcon.classList.add(\"hidden\");\r\n    }\r\n  });\r\n\r\n  // Toggle the clicked accordion item\r\n  accordionItem.classList.toggle(\"active\");\r\n\r\n  if (accordionItem.classList.contains(\"active\")) {\r\n    accordionBody.style.maxHeight = accordionBody.scrollHeight + \"px\";\r\n    plusIcon.classList.add(\"hidden\");\r\n    minusIcon.classList.remove(\"hidden\");\r\n  } else {\r\n    accordionBody.style.maxHeight = null;\r\n    plusIcon.classList.remove(\"hidden\");\r\n    minusIcon.classList.add(\"hidden\");\r\n  }\r\n}\r\n\r\n// Optionally, open the first accordion by default\r\nif (accordionItems.length > 0) {\r\n  accordionItems[0].classList.add(\"active\");\r\n  accordionItems[0].nextElementSibling.style.maxHeight =\r\n    accordionItems[0].nextElementSibling.scrollHeight + \"px\";\r\n  accordionItems[0].querySelector(\".plus\").classList.add(\"hidden\");\r\n  accordionItems[0].querySelector(\".minus\").classList.remove(\"hidden\");\r\n}\r\n\r\n// Attach event listeners to each accordion button\r\naccordionItems.forEach((item) =>\r\n  item.addEventListener(\"click\", accordionDropdown)\r\n);\r\n\n\n//# sourceURL=webpack://starter/./src/js/accordion.js?");

/***/ }),

/***/ "./src/js/form.js":
/*!************************!*\
  !*** ./src/js/form.js ***!
  \************************/
/***/ (() => {

eval("function initializeForm(formSelector) {\r\n    const form = document.querySelector(formSelector);\r\n    const requiredFields = form.querySelectorAll('.required');\r\n\r\n    // Dropdowns\r\n    const bankaDropdown = form.querySelector('#banka');\r\n    const mestoDropdown = form.querySelector('#mesto');\r\n    const adresaDropdown = form.querySelector('#adresa');\r\n\r\n    if (bankaDropdown) {\r\n        bankaDropdown.addEventListener('change', async () => {\r\n            if (bankaDropdown.value) {\r\n                mestoDropdown.disabled = false;\r\n                // Simulating API call\r\n                const response = await fetch(`api/mesto/${bankaDropdown.value}`);\r\n                const data = await response.json(); // Example: [{ id: 'mesto1', name: 'Grad 1' }]\r\n\r\n                mestoDropdown.innerHTML = '<option value=\"\">Izaberite grad</option>';\r\n                data.forEach((item) => {\r\n                    const option = document.createElement('option');\r\n                    option.value = item.id;\r\n                    option.textContent = item.name;\r\n                    mestoDropdown.appendChild(option);\r\n                });\r\n            } else {\r\n                mestoDropdown.disabled = true;\r\n                adresaDropdown.disabled = true;\r\n                mestoDropdown.innerHTML = '<option value=\"\">Izaberite grad</option>';\r\n                adresaDropdown.innerHTML = '<option value=\"\">Izaberite ekspozituru</option>';\r\n            }\r\n        });\r\n    }\r\n\r\n    if (mestoDropdown) {\r\n        mestoDropdown.addEventListener('change', async () => {\r\n            if (mestoDropdown.value) {\r\n                adresaDropdown.disabled = false;\r\n                // Simulating API call\r\n                const response = await fetch(`api/adresa/${mestoDropdown.value}`);\r\n                const data = await response.json(); // Example: [{ id: 'adresa1', name: 'Ekspozitura 1' }]\r\n\r\n                adresaDropdown.innerHTML = '<option value=\"\">Izaberite ekspozituru</option>';\r\n                data.forEach((item) => {\r\n                    const option = document.createElement('option');\r\n                    option.value = item.id;\r\n                    option.textContent = item.name;\r\n                    adresaDropdown.appendChild(option);\r\n                });\r\n            } else {\r\n                adresaDropdown.disabled = true;\r\n                adresaDropdown.innerHTML = '<option value=\"\">Izaberite ekspozituru</option>';\r\n            }\r\n        });\r\n    }\r\n\r\n    form.addEventListener('submit', (e) => {\r\n        e.preventDefault();\r\n        let isValid = true;\r\n\r\n        requiredFields.forEach((field) => {\r\n            const errorElement = form.querySelector(`#${field.id}Error`);\r\n            if (!field.value || (field.type === 'checkbox' && !field.checked)) {\r\n                errorElement.textContent = 'Ovo polje je obavezno';\r\n                isValid = false;\r\n            } else if (field.id === 'email') {\r\n                const emailRegex = /^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$/;\r\n                if (!emailRegex.test(field.value)) {\r\n                    errorElement.textContent = 'Email nije u dobrom formatu';\r\n                    isValid = false;\r\n                } else {\r\n                    errorElement.textContent = '';\r\n                }\r\n            } else {\r\n                errorElement.textContent = '';\r\n            }\r\n        });\r\n\r\n        if (isValid) {\r\n            alert('Form submitted successfully!');\r\n        }\r\n    });\r\n}\r\n\r\n// Initialize for multiple forms\r\ninitializeForm('#dynamicForm'); // Large form\r\ninitializeForm('#smallForm');  // Small form (if exists)\r\n\n\n//# sourceURL=webpack://starter/./src/js/form.js?");

/***/ }),

/***/ "./src/js/navigation.js":
/*!******************************!*\
  !*** ./src/js/navigation.js ***!
  \******************************/
/***/ (() => {

eval("// Navigation hamburger menu\r\n\r\nconst hamburgerButton = document.querySelector(\".js-button-menu\");\r\nhamburgerButton.addEventListener(\"click\", toggleNav);\r\nconst hamburgerButtonImg = document.querySelector(\".js-button-menu-img\");\r\n\r\nfunction toggleNav() {\r\n  const nav = document.querySelector(\"#nav\");\r\n  nav.classList.toggle(\"nav--open\");\r\n\r\n  // const buttonHeaderMobile = document.querySelector(\".js-button-header\");\r\n  // buttonHeaderMobile.classList.toggle(\"button--header-mobile\");\r\n\r\n  const body = document.querySelector(\"body\");\r\n  body.classList.toggle(\"no-scroll\");\r\n\r\n\r\n  hamburgerButtonImg.src = hamburgerButtonImg.src.includes(\"close.svg\")\r\n    ? \"./assets/images/hamburger.svg\"\r\n    : \"./assets/images/close.svg\";\r\n}\r\n\r\n// Navigation dropdown\r\n\r\nconst navItem = document.querySelector(\".js-nav-item\");\r\nnavItem.addEventListener(\"click\", toggleNavItem);\r\n\r\n\r\nfunction toggleNavItem() {\r\n  const dropdown = document.querySelector(\".js-dropdown\");\r\n  dropdown.classList.toggle(\"dropdown--show\");\r\n}\n\n//# sourceURL=webpack://starter/./src/js/navigation.js?");

/***/ }),

/***/ "./src/js/slider.js":
/*!**************************!*\
  !*** ./src/js/slider.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _cards_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../cards.json */ \"./cards.json\");\n\r\n\r\n// Slider\r\n\r\nconst arrows = document.querySelectorAll(\".js-slider-arrow\");\r\nconst carousel = document.querySelector(\".js-slider-carousel\");\r\n\r\n_cards_json__WEBPACK_IMPORTED_MODULE_0__.forEach((card) => createCard(card));\r\n\r\nconst slides = document.querySelectorAll(\".slider__card\");\r\n\r\nfunction createCard(card) {\r\n  let sliderCard = document.createElement(\"div\");\r\n  sliderCard.classList.add(\"slider__card\");\r\n\r\n  let sliderImageContainer = document.createElement(\"div\");\r\n  sliderImageContainer.classList.add(\"slider__img-container\");\r\n\r\n  let sliderImage = document.createElement(\"img\");\r\n  sliderImage.classList.add(\"slider__img\");\r\n  sliderImage.src = card.path;\r\n  sliderImage.setAttribute(\"alt\", card.name);\r\n\r\n  let sliderOverlay = document.createElement(\"div\");\r\n  sliderOverlay.classList.add(\"slider__overlay\");\r\n\r\n  let overlayButton = document.createElement(\"button\");\r\n  overlayButton.classList.add(\"slider__overlay-button\");\r\n\r\n  let phoneIcon = document.createElement(\"img\");\r\n  phoneIcon.classList.add(\"slider__overlay-icon\");\r\n  phoneIcon.src = \"./assets/images/phone.svg\";\r\n  phoneIcon.setAttribute(\"alt\", \"phone icon\");\r\n  overlayButton.append(phoneIcon);\r\n  sliderOverlay.append(overlayButton);\r\n  sliderImageContainer.append(sliderImage);\r\n  sliderImageContainer.append(sliderOverlay);\r\n\r\n  let sliderName = document.createElement(\"p\");\r\n  sliderName.classList.add(\"slider__name\");\r\n  sliderName.innerHTML = card.name;\r\n\r\n  let sliderPosition = document.createElement(\"p\");\r\n  sliderPosition.classList.add(\"slider__position\");\r\n  sliderPosition.innerHTML = card.position;\r\n\r\n  sliderCard.append(sliderImageContainer);\r\n  sliderCard.append(sliderName);\r\n  sliderCard.append(sliderPosition);\r\n\r\n  carousel.append(sliderCard);\r\n}\r\n\r\nconst firstCard = slides[0];\r\nfirstCard.classList.add(\"slider__card--first\");\r\narrows[0].disabled = true;\r\n\r\narrows.forEach((arrow) => {\r\n  arrow.addEventListener(\"click\", () => {\r\n    let isEndReached = false;\r\n    let firstCardWidth = firstCard.clientWidth + 20;\r\n\r\n    carousel.scrollLeft +=\r\n      arrow.id == \"left\" ? -firstCardWidth : firstCardWidth;\r\n\r\n    isEndReached =\r\n      arrow.id == \"right\"\r\n        ? carousel.scrollLeft + carousel.clientWidth === carousel.scrollWidth\r\n        : false;\r\n\r\n    arrows[0].disabled = carousel.scrollLeft === 0;\r\n    arrows[1].disabled = isEndReached;\r\n  });\r\n});\n\n//# sourceURL=webpack://starter/./src/js/slider.js?");

/***/ }),

/***/ "./cards.json":
/*!********************!*\
  !*** ./cards.json ***!
  \********************/
/***/ ((module) => {

"use strict";
eval("module.exports = /*#__PURE__*/JSON.parse('[{\"image\":\"image_1\",\"path\":\"./assets/images/image-1.svg\",\"name\":\"Tom Parenteau\",\"position\":\"Tom Parenteau\"},{\"image\":\"image_2\",\"path\":\"./assets/images/image-2.svg\",\"name\":\"Sarah Pillmore\",\"position\":\"CPO\"},{\"image\":\"image_3\",\"path\":\"./assets/images/image-3.svg\",\"name\":\"Sugosh Venkataraman\",\"position\":\"Director of Engineering\"},{\"image\":\"image_4\",\"path\":\"./assets/images/image-4.svg\",\"name\":\"Dr David Edington\",\"position\":\"CTO\"},{\"image\":\"image_6\",\"path\":\"./assets/images/image-5.svg\",\"name\":\"Paul Dixon\",\"position\":\"Chief Financial Officer\"},{\"image\":\"image_5\",\"path\":\"./assets/images/image-6.svg\",\"name\":\"Connor Campbell\",\"position\":\"CEO and co-founder\"},{\"image\":\"image_7\",\"path\":\"./assets/images/image-7.svg\",\"name\":\"Laura Baers\",\"position\":\"Chief of Staff\"},{\"image\":\"image_8\",\"path\":\"./assets/images/image-8.svg\",\"name\":\"Michael Kew\",\"position\":\"Head of Product\"},{\"image\":\"image_9\",\"path\":\"./assets/images/image-9.svg\",\"name\":\"Sarah Malin\",\"position\":\"Head of Delivery\"},{\"image\":\"image_10\",\"path\":\"./assets/images/image-10.svg\",\"name\":\"Lester Gleeson\",\"position\":\"Head of Quality\"}]');\n\n//# sourceURL=webpack://starter/./cards.json?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	__webpack_require__("./src/scss/style.scss");
/******/ 	__webpack_require__("./src/js/accordion.js");
/******/ 	__webpack_require__("./src/js/form.js");
/******/ 	__webpack_require__("./src/js/navigation.js");
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/slider.js");
/******/ 	
/******/ })()
;